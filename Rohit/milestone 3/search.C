#include <stdio.h>

void main(){
    int a[100], i, num, n, search=0; 
    
    printf("Enter the size of array");
    scanf("%d", &n);
    
    printf("Enter the elements in array");
    for(i=0; i<n; i++){
        scanf("%d", &a[i]);
    }
    
    printf("Enter the element to search");
    scanf("%d", &num);
    
    for(i=0; i<n; i++){
        if(a[i] == num){
            search = 1;
            break;
        }
    }
    
    if(search == 1){
        printf("The number is found at position %d", i+1);
    }
    else{
        printf("The number is not in the array");
    }
}