#include <stdio.h>
#include <conio.h>

void main() {
    float h, b, area;

    printf("Enter height and base: ");
    scanf("%f %f", &h, &b);
    area = (h * b)/2 ;
    printf("Area of triangle: %f",area);
    getch();
}
