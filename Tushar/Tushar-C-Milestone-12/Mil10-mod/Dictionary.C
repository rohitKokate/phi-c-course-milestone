#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#define Max_limit 50

struct meaning
{
	char word[20];
	char mean[20];
};

void main ()
{
	struct meaning dic[100];
	int ch,i=-1;
	char temp[20];
	FILE *fDic;
	while(1)
	{
		printf("\n\n 1.Add word and its meaning");
		printf("\n 2.Delete a word");
		printf("\n 3.Search a word");
		printf("\n 4.Print");
		printf("\n 5.Save the dictionary to a file");
		printf("\n 6.Load the dictionary from a file");
		printf("\n 7.Exit\n");
		scanf("%d",&ch);

		switch(ch)
		{
			case 1 : printf("\n Enter a word and its meaning: ");
						i++;
						scanf("%s%s",&dic[i].word,&dic[i].mean);
						printf("\nAdded");
						break;
			case 2 : printf("\nEnter a word to be deleted: ");
						scanf("%s",&temp);
						for(int j=0;i<=i;j++)
						{
							if(strcmp(temp,dic[j].word))
							{
								for(int k=j-1;k<i;k++)
								{
									strcpy(dic[j].word,dic[j+1].word);
									strcpy(dic[j].mean,dic[j+1].mean);
								}
								printf("\nDeleted");
								strcpy(dic[i].word,"");
								strcpy(dic[i].mean,"");
								i--;
								break ;
							}
						}
						break;

			case 3 : printf("\nEnter a word to be searched: ");
						scanf("%s",&temp);
						for(int j1=0;j1<=i;j1++)
						{
							if(strcmp(temp,dic[j1].word)==0)
							{
								printf("\nMeaning of %s is: %s",dic[j1].word,dic[j1].mean);
								break;
							}
						}
						break;

			case 4 : printf("\nWords are: ");
						for(int j2=0;j2<=i;j2++)
						{
							printf("\n %s --> %s",dic[j2].word,dic[j2].mean);
						}
						break;
						
			case 5 : fDic = fopen("Dictionary.txt","w");
				    fprintf(fDic,"Words are:\n");
				    for(int j2=0 ; j2<=i ; j2++)
					 {
					       fprintf(fDic,"\n%s --> %s\n",dic[j2].word,dic[j2].mean);
					 }
					 fclose(fDic);
					 break ;

		    case 6 : fDic = fopen("Dictionary.txt","r");
				    char singleLine[150];
				    while(!feof(fDic)){
				        fgets(singleLine,150,fDic);
				        puts(singleLine);
				    }
				    fclose(fDic);
					 break ;

			case 7 : exit(0);
			         break;
            
            default : printf("\nPlease choose a correct option!!");
		}
	}
	 getch();
}