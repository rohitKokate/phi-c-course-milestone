#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 

void MoveNode(struct List** dest_ref, struct List** source_ref){
    struct List* newNode = *source_ref;
    assert(newNode != NULL);
    *source_ref = newNode->next; 
    newNode->next = *dest_ref; 
    *dest_ref = newNode;
    
}

void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* a = NULL;
    struct List* b = NULL;
    
    push(&a, 5); 
    push(&a, 2); 
    push(&a, 1);  
    push(&b, 3); 
    push(&b, 5);
    push(&b, 5);
    
    MoveNode(&a,&b);
     
    printf("Node moved successfully" ); 
} 