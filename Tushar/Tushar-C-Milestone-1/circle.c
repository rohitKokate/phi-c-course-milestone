#include <stdio.h>
#include <conio.h>
#define pi 3.141592

void main() {
    float radius, area;

    printf("Enter radius: ");
    scanf("%f", &radius);
    area = pi * radius * radius ;
    printf("Area of circle: %f",area );
    getch();
}
