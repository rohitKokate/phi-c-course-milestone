#include <algorithm>
#include <iostream>
#include <array>

using namespace std;

void ascending(array<int, 10> &a) {
	sort(a.begin(), a.end());

	for (int i = 0; i < a.size(); ++i)
	{
		cout<<a[i]<<"\n";
	}
}

void descending(array<int, 10> &a) {
	sort(a.begin(), a.end(), greater<int>());

	for (int i = 0; i < a.size(); ++i)
	{
		cout<<a[i]<<"\n";
	}
}

int main(int argc, char const *argv[])
{
	/* code */
	array<int, 10> a;
	int choice;

	for (int i = 0; i < a.size(); ++i)
	{
		cin>>a[i];
	}

	cout<<"Choose sorting order: \n1.Ascending\n2.Descending";
	cout<<"\nEnter choice: \n";
	cin>>choice;

	switch(choice) {
		case 1: ascending(a);
				break;

		case 2: descending(a);
				break;

		default: exit(0);
		}

	return 0;
}