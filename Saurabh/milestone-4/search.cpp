// Create a program to input a set of integers and then search the array to see if a particular integer exists in the array

#include <algorithm>
#include <iostream>
#include <array>

using namespace std;

void add(array<int, 10> &a) {
	int num; 
	cin>>num;

	for (int i = 0; i < a.size(); ++i){
		if(a[i]==0){
			a[i]=num;
		}
	}
}

void search_array(array<int, 10> &a) {
	int  num1;
	bool flag = false;

	cout<<"Enter number to search: ";
	cin>>num1;

	for (int i = 0; i < a.size(); ++i) {
		if (num1 == a[i]){
			flag = true;
			break;
		}
	}

	if (flag == true){
		cout<<"The number found";
	}else {
		cout<<"The number not found";
	}
}

void remove_array(array<int, 10> &a) {
	int num2, remove;
	cin>>num2;
	for (int i = 0; i < a.size(); ++i){
		if(a[i]==num2){
			remove=a[i];
		}else {
			cout<<"Not removed";
		}
	}
}


void ascending(array<int, 10> &a) {
	sort(a.begin(), a.end());

	for (int i = 0; i < a.size(); ++i)
	{
		cout<<a[i]<<"\n";
	}
}

void descending(array<int, 10> &a) {
	sort(a.begin(), a.end(), greater<int>());

	for (int i = 0; i < a.size(); ++i)
	{
		cout<<a[i]<<"\n";
	}
}

void sort_array(array<int, 10> &a) {
	int choice;
	cout<<"Choose sorting order: \n1.Ascending\n2.Descending\n3.exit";
	cout<<"\nEnter choice: \n";
	cin>>choice;

	switch(choice) {
		case 1: ascending(a);
				break;

		case 2: descending(a);
				break;

		case 3: exit(0);
				break;
		}
}

void print_array(array<int, 10> &a) {
	for (int i = 0; i < a.size(); ++i)
	{
		cout<<a[i]<<"\n";
	}
}

int main(int argc, char const *argv[])
{
	array<int, 10> a;
	int choice;

	for (int i = 0; i < a.size(); ++i)
	{
		cin>>a[i];
	}

	for (int i = 0; i < a.size(); ++i)
	{
		cout<<a[i]<<"\n";
	}

	cout<<"Enter choice: \n1.add element\n2.search element\n3.remove element\n4.sort array\n5.print array\n6.exit";
	cin>>choice;

	while(true) {
		switch(choice) {
			case 1: add(a);
					break;
			case 2: search_array(a);
					break;
			case 3: remove_array(a);
					break;
			case 4: sort_array(a);
					break;
			case 5: print_array(a);
					break;
			case 6: exit(0);
					break;
		}
	}

	return 0;
}