#include <stdio.h>
#ifndef PI
#define PI 3.14
#endif

int main(int argc, char const *argv[])
{
	/* code */
	float r, area;
	printf("Enter radius of circle: ");
	scanf("%f", &r);

	area = PI * (r*r);
	printf("The area of circle: %f", area);

	return 0;
}