#include <stdio.h>

int main(int argc, char const *argv[])
{
	/* code */
	float area, l, w;
	printf("Enter values of length: ");
	scanf("%f", &l);
	printf("Enter values of width: ");
	scanf("%f", &w);

	area = l * w;
	printf("The area of rectangle: %0.2f", area);

	return 0;
}