#include <stdio.h>
// #include <.h>

int main() {
	float pound, kg;
	printf("The weight in pounds: ");
	scanf("%f", &pound);

	kg = pound * 0.45;

	printf("The weight in kg: %f", kg);
	return 0;
}