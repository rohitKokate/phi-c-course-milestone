#include <stdio.h>

int main(int argc, char const *argv[])
{
	float fahrenheit, celcius;
	printf("Enter temp in fahrenheit: ");
	scanf("%f", &fahrenheit);

	celcius = ((fahrenheit - 32) / 1.8000);

	printf("The temp in celcius: %f", celcius);

	return 0;
}