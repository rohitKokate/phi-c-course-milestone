#include <stdio.h>

int main(int argc, char const *argv[])
{
	/* code */
	float area, base, height;
	printf("Enter base of triangle: ");
	scanf("%f", base);
	printf("Enter height of triangle: ");
	scanf("%f", height);

	area = 0.5 * base * height;
	printf("The are of triangle: %f", area);

	return 0;
}