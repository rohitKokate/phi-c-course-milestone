#include <stdio.h>

int main(int argc, char const *argv[])
{
	float inches, cm; 
	printf("Enter length in inches: ");
	scanf("%f", &inches);

	cm = inches * 2.54;

	printf("The length in centimeters: %0.2f cm", cm);	

	return 0;
}