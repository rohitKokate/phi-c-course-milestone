#include <iostream>

using namespace std;

int gcd_naive(int a, int b) {
  int x;

  if(b==0){
    return a;
  }
    x = a%b;
    return gcd_naive(b, x);
 
}

int main() {
  int a, b;
  cin >> a >> b;
  cout << gcd_naive(a, b) << std::endl;
  return 0;
}