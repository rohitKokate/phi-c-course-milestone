// double pow(double x, int n) - Calculates and returns xn
#include <iostream>
#include <math.h>

using namespace std;

double power(double x, int n) {
	double a;

	a = pow(x, double(n));

	cout<<"The answer: "<<a;

	return 0;
}

int main(int argc, char const *argv[])
{
	/* code */
	int n;
	double x;

	cout<<"Enter x: ";
	cin>>x;
	cout<<"Enter n: ";
	cin>>n;

	power(x, n);

	return 0;
}