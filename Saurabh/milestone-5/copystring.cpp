#include <iostream>
#include <string>

using namespace std;

void str_copy(char *str1, char *str2) {
	int l1 = 0;
	int l2 = 0;

	while(*str1 != '\0') {
		l1++;
		str1++;
	}

	while(*str2 != '\0') {
		l2++;
		str2++;
	}

	for(int i=0; i < l2; i++){
		str1[l1] = str2[i];
		l1++;
	}

	cout<<str1;
}

int main(int argc, char const *argv[])
{	
	char str1[100], str2[100];
	fgets(str1, sizeof(str1), stdin);
	fgets(str2, sizeof(str2), stdin);

	// copy(str2, str1.size(), 0);

	str_copy(str1, str2);

	return 0;
}