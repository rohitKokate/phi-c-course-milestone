#include <iostream>
#include <string>

using namespace std;

void str_find_substring(char* s1, char* s2) {

	int l = 0;
	while(*s1 != '\0') {
		l++;
		s1++;
	}

	for(int i=0; i < l; i++){
		if(s1[i]==s2[i]){
			cout<<"1";
		}else {
			cout<<"-1";
		}
	}
}

int main(int argc, char const *argv[])
{
	char str1[1000], str2[1000];
	fgets(str1, sizeof(str1), stdin);
	fgets(str2, sizeof(str2), stdin);

	str_find_substring(str1, str2);
	return 0;
}