// int str_length(char *str) - returns the length of the string or -1 on error
#include <iostream>

using namespace std

int str_length(char *str)
{
	if(*str != '\0')
	{
		int len= strlen(str);
		printf("The length of string is: %d\n",len);
	}
	else
	{
		return -1;
	}
	return 0;
}

int main(int argc, char const *argv[])
{
	char str[20]="hello how are you";
	str_length(str);
	return 0;
}