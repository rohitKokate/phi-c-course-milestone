#include<stdio.h> 
#include<stdlib.h> 
#include<assert.h> 
  
struct List 
{ 
    int data; 
    struct List* next; 
}; 
  
int Pop(struct List** head_ref) 
{ 

   struct List* head; 
   int result;
   head = *head_ref;
   assert(head!=NULL);
   result = head->data;
   *head_ref = head->next;
   free(head);
   return(result);
} 
  
void push(struct List** head_ref, int new_data) 
{ 
    struct List* new_node = 
            (struct List*) malloc(sizeof(struct List)); 
    new_node->data  = new_data; 
    new_node->next = (*head_ref); 
    (*head_ref)    = new_node; 
} 
  
int main() 
{ 
    struct List* head = NULL; 
    push(&head, 5); 
    push(&head, 2); 
    push(&head, 1);  
    push(&head, 3); 
    push(&head, 5);
    push(&head, 5);
     
       
     
    printf("The data extracted is %d ",Pop(&head)); 
} 