#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
int addNumber(int a[],int size,int i);
int searchNumber(int num,int a[]);
int removeNumber(int a[],int num,int i);
int sortNumber(int a[],int i);

int a[100],size=100;

void main()
{
	int ch,i=0,n,x;
	FILE *fArray;
	printf("Enter the number of elements in array: ");
	scanf("%d",&n);
	printf("Enter array elements: ");
	for(x=0;x<n;x++)
	{
		scanf("%d",&a[x]);
		i++;
	}

 while(1)
 {
Loop:	printf("\n\n1.Add element to the array");
	printf("\n2.Search an element in the array");
	printf("\n3.Delete any element from the array");
	printf("\n4.Sort the array elements");
	printf("\n5.Print all the elements in the array");
	printf("\n6.Save the array elements to a file");
	printf("\n7.Load the array elements from the file");
	printf("\n8.Exit\n");
	scanf("%d",&ch);

	switch(ch)
	{
		 case 1 : i = addNumber(a ,size,i);
					 break ;
		 case 2 : printf("\nEnter a number you want to search: ");
					 int num;
					 scanf("%d",&num);
					 int j = (searchNumber(num ,a));
					 if(j==0)
					 {
						  printf("\nElement doesn't exist!!");
					 }
					 else
					 {
						  printf("\nElement is found at position %d ",j);
					 }
					 break ;
		 case 3 : printf("\nEnter a number you want to delete: ");
					 int num1;
					 scanf("%d",&num1);
					 i = removeNumber(a , num1,i);
					 break;
		 case 4 : sortNumber(a,i);
					 break;

		 case 5 : printf("\nThe elements in the array are:\n");
					for(int k=0 ; k<i ; k++)
					 {
						printf("%d " ,a[k]);
					 }
					 break ;

		 case 6 : fArray = fopen("Array.txt","w");
				fprintf(fArray,"Array elements are:\n");
				for(int k=0 ; k<i ; k++)
					 {
					       fprintf(fArray,"%d " ,a[k]);
					 }
					 fclose(fArray);
					 break ;

		 case 7 : fArray = fopen("Array.txt","r");
				    char singleLine[150];
				    while(!feof(fArray)){
				        fgets(singleLine,150,fArray);
				        puts(singleLine);
				    }
				    fclose(fArray);
					 break ;
					 
		case 8 : exit(0);
					 break;

		 default : printf("\nPlease choose a correct option!!");
					  goto Loop;

	}
 }
	getch();
}

int addNumber(int a[] , int size ,int i)
{
	 int num;
	 printf("\nEnter a number you want to add: ");
	 scanf("%d",&num);
	 if(searchNumber(num ,a)!=0)
	 {
		  printf("\nEntered number already exists!!");
	 }
	 else
	 {
		  a[i] = num;
		  i++;
		  printf("\nNumber sucessfully added!!");
	 }

	 return i ;

}

int searchNumber(int num , int a[])
{
	 for (int j=0 ;j<size;j++)
	 {
		  if(a[j]==num)
		  {
				return j+1;
		  }
	 }

	 return 0;
}

int removeNumber(int a[] ,int num,int i)
{
	 for(int j=0 ; j<i;j++)
	 {
		  if(a[j]==num)
		  {
				printf("\nNumber has been sucessfully deleted!!");
				for(int k=j;k<i-1;k++)
				{
					 a[k] = a[k+1];
				}
				i--;
				break;
		  }
	 }
	 return i;
}

int sortNumber(int a[],int i)
{
	 printf("\n1.Ascending Order");
	 printf("\n2.Descending Order\n");
	 int ch;
	 scanf("%d",&ch);
	 if(ch==1)
	 {
		for(int j=0 ; j<i ; j++)
		{
		  for(int k=j ; k<i ;k++)
		  {
				if(a[j]>a[k])
				{
					 int temp = a[j];
					 a[j] = a[k];
					 a[k] = temp;
				}
		  }
		}
		printf("\nAscending Order: ");
		for(int j=0;j<i;j++)
		{
			printf(" %d ",a[j]);
		}
	}
	else
	{
		for(int j=0 ; j<i ; j++)
		{
		  for(int k=j ; k<i ;k++)
		  {
				if(a[j]<a[k])
				{
					 int temp = a[j];
					 a[j] = a[k];
					 a[k] = temp;
				}
		  }
		}
		printf("\nDescending Order: ");
		for(int j=0;j<i;j++)
		{
			printf(" %d ",a[j]);
		}
	}
}